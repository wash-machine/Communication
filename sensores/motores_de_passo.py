#!/usr/bin/python
#
import sys
import time
import signal
import RPi.GPIO as GPIO
import socket
import os
import threading

############################### PINOS ####################################
#       3.3 V                1  2           5 V
#                            3  4           5 V
#                            5  6           GND
#                            7  8
#       GND                  9  10
#       NIVEL_1              11 12          SAPATO_1
#       NIVEL_2              13 14          GND
#       MOTOR_DC_1           15 16          CHAVE_1
#       3.3 V                17 18          CHAVE_2
#                            19 20          GND
#                            21 22          SAPATO_2
#                            23 24
#       GND                  25 26
#       BOMBA_3              27 28          BOMBA_2
#       BOMBA_1              29 30          GND
#       MOTOR1_A             31 32          MOTOR2_A
#       MOTOR1_B             33 34          GND
#       MOTOR1_C             35 36          MOTOR2_B
#       MOTOR1_D             37 38          MOTOR2_C
#       GND                  39 40          MOTOR2_D
#

### SETA A PINAGEM DA PLACA MODO BCM -> ###
###         VER PINOS COM O COMANDO gpio readall ####
GPIO.setmode(GPIO.BCM)

### FINALIZA O PROGRAMA DE FORMA SEGURA ###
def clean():
    setStep_1(0,0,0,0)
    setStep_2(0,0,0,0)
    ativar_motor_esc(False)
    ativar_bombas(True, False)
    ativar_bombas(False, False)
    GPIO.cleanup()

def sigint_handler(signum, instant):
    clean()
    time.sleep(0.5)
    sys.exit()

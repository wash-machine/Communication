#!/usr/bin/python
#
import sys
import time
import signal
import RPi.GPIO as GPIO
import socket
import os
import threading

############################### PINOS ####################################
#       3.3 V                1  2           5 V
#                            3  4           5 V
#                            5  6           GND
#                            7  8
#       GND                  9  10
#       NIVEL_1              11 12          SAPATO_1
#       NIVEL_2              13 14          GND
#       MOTOR_DC_1           15 16          CHAVE_1
#       3.3 V                17 18          CHAVE_2
#                            19 20          GND
#                            21 22          SAPATO_2
#                            23 24
#       GND                  25 26
#       BOMBA_3              27 28          BOMBA_2
#       BOMBA_1              29 30          GND
#       MOTOR1_A             31 32          MOTOR2_A
#       MOTOR1_B             33 34          GND
#       MOTOR1_C             35 36          MOTOR2_B
#       MOTOR1_D             37 38          MOTOR2_C
#       GND                  39 40          MOTOR2_D
#

### SETA A PINAGEM DA PLACA MODO BCM -> ###
###         VER PINOS COM O COMANDO gpio readall ####
GPIO.setmode(GPIO.BCM)

### FINALIZA O PROGRAMA DE FORMA SEGURA ###
def clean():
    setStep_1(0,0,0,0)
    setStep_2(0,0,0,0)
    ativar_motor_esc(False)
    ativar_bombas(True, False)
    ativar_bombas(False, False)
    GPIO.cleanup()

def sigint_handler(signum, instant):
    clean()
    time.sleep(0.5)
    sys.exit()

### SENSOR DE DISTANCIA ###
sapato_1 = 18
sapato_2 = 25

GPIO.setup(sapato_1, GPIO.IN)
GPIO.setup(sapato_2, GPIO.IN)

GPIO.add_event_detect(sapato_1, GPIO.FALLING, callback=sensor_presenca_1, bouncetime=300)
GPIO.add_event_detect(sapato_2, GPIO.FALLING, callback=sensor_presenca_2, bouncetime=300)

### SENSOR DE PRESANCA ####
def sensor_presenca_1(channel):
    global sensor1
    if(GPIO.input(sapato_1) == GPIO.HIGH):
        print("calcado fora do local. Por favor, inserir novamente")
    else:
        print("error sensor presensa 1")
        print("Erro solucionado. Ativar sistema")
        sensor1 = True

def sensor_presenca_2(channel):
    global sensor2
    if(GPIO.input(sapato_2) == GPIO.HIGH):
        print("calcado fora do local. Por favor, inserir novamente")
    else:
        print("error sensor presensa 2")
        print("Erro solucionado. Ativar sistema")
        sensor2 = True


if __name__ == "__main__":
    sensor1=False
    sensor2=False
    for i in range(100000):
        if not sensor1 and not sensor2:
            break
        time.sleep(0.5)
    assert sensor1
    assert sensor2

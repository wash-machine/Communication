from urllib.request import urlopen, Request
from urllib.error import HTTPError
from urllib import parse

import time
import json

SERVICE_ORDER_FILE = '/home/pi/.shoe_washer_so.conf'
SHOE_WASHER_FILE = '/home/pi/.shoe_washer.conf'

EM_ESPERA = 0
ESCOVANDO = 1
ENXAGUANDO = 2
SECANDO = 3

class MissinId(Exception):
    pass

def save_service_order_id(id):
    with open(SERVICE_ORDER_FILE, 'w') as config:
        config.write(str(id))

def load_service_order_id():
    return load_id(SERVICE_ORDER_FILE)


def load_machine_id():
    return load_id(SHOE_WASHER_FILE)

def load_id(file_path):
    try:
        with open(file_path, 'r') as config:
            data = config.read()
            id = int(data)
            return id

    except FileNotFoundError as error:
        raise MissinId('Config file not fould in {}'.format(file_path))
    except ValueError as error:
        raise MissinId('Invalid data in configuration, fail to parse content to Int')
    except Exception as error:
        print(error)


def load_url(request):
    try:
        response = urlopen(request)
        content = response.read()
        if 200 <= response.status < 300:
            return content
    except HTTPError as e:
        content = e.read().decode('utf-8')
        print('HTTP {} - {}'.format(e.code, content))
        return None


def start_wash():
    machine_id = load_machine_id()
    url = 'http://192.168.43.253:8000/api/service_order/start_wash/{}/'.format(machine_id)
    request = Request(url, method='PUT')
    try:
        response = urlopen(request)
        if 200 <= response.status < 300:
            print('Started wash')
            content = response.read().decode('utf-8')
            dict_id = json.loads(content)
            service_order_id = dict_id['service_order_id']
            save_service_order_id(service_order_id)
        else:
            print('start_wash service not started')
    except Exception as error:
        print(error.read())


def update_state(state):
    service_order_id = load_service_order_id()
    url = 'http://192.168.43.253:8000/api/service_order/update_state/{}/{}/'.format(
        service_order_id, state
    )
    request = Request(url, method='PUT')
    data = load_url(request)
    print('update_state', data)


def send_error(error):
    id = load_machine_id()
    url = 'http://192.168.43.253:8000/api/service_order/get_errors/{}/'.format(id)
    data = json.dumps({"error_code": error}).encode('utf-8')
    request = Request(url,
        data=data,
        headers={
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        method='PUT')
    content = load_url(request)
    print('send_error', content)


def clean_error():
    machine_id = load_machine_id()
    url = 'http://192.168.43.253:8000/api/service_order/get_errors/{}/'.format(machine_id)
    request = Request(url, method='GET')
    content = load_url(request)
    print('clear_error', content)

if __name__ == '__main__':
    wait_time = 8
    print('start')
    start_wash()
    time.sleep(wait_time)
    #print('update to 2')
    update_state(ESCOVANDO)
    time.sleep(wait_time)
    #print('update to 3')
    update_state(ENXAGUANDO)
    time.sleep(wait_time)
    #print('update to 4')
    update_state(SECANDO)
    time.sleep(wait_time)
    #print('update to 1')
    update_state(EM_ESPERA)
    time.sleep(wait_time)
    send_error(0)
    time.sleep(wait_time)
    send_error(1)
    time.sleep(wait_time)
    send_error(2)
    time.sleep(wait_time*2)
    clean_error()
    print('done')

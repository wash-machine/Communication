#!/usr/bin/python
#
import socket
import os

aberto = 0

host = '192.168.25.215'
port = 7000
addr = (host, port)
serv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serv_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serv_socket.bind(addr)
serv_socket.listen(10)
while not aberto:
        print 'aguardando conexao'
        con, cliente = serv_socket.accept()
        print 'conectado'
        print "aguardando mensagem"
        recebe = con.recv(1024)
        print "mensagem recebida: " + recebe
        if recebe == "q":
            aberto = 1
        else:
            text = "python ../I2C/raspi2c.py " + recebe
            os.system(text)

print "Fechando socket"
serv_socket.close()
print "Servidor finalizado!"

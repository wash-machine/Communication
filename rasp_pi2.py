#!/usr/bin/python
#
import sys
import time
import signal
import RPi.GPIO as GPIO
import socket
import os
import threading
from TCP import request
from TCP.request import EM_ESPERA, ESCOVANDO, ENXAGUANDO, SECANDO

############################### PINOS ####################################
#       3.3 V                1  2           5 V
#                            3  4           5 V
#                            5  6           GND
#                            7  8
#       GND                  9  10
#       NIVEL_1              11 12          SAPATO_1
#       VALVULA_ELETRONICA   13 14          GND
#       MOTOR_DC_1           15 16          CHAVE_1
#       3.3 V                17 18          INITIALLIZE_SETUP
#                            19 20          GND
#                            21 22          SAPATO_2
#                            23 24
#       GND                  25 26
#       BOMBA_3              27 28          BOMBA_2
#       BOMBA_1              29 30          GND
#       MOTOR1_A             31 32          MOTOR2_A
#       MOTOR1_B             33 34          GND
#       MOTOR1_C             35 36          MOTOR2_B
#       MOTOR1_D             37 38          MOTOR2_C
#       GND                  39 40          MOTOR2_D
#


#### GLOBAL VARIABLES ####

actually_steps_motor_1 = 0
actually_steps_motor_2 = 0
mode_pump = False
state_pump = False
mode_motor_dc = False

### ERRORS ###

NIVEL_AGUA = 0
NIVEL_SABAO = 1
SAPATO_DIREITA = 2
SAPATO_ESQUERDA= 3
TRILHO_FORA = 4
TAMPA_ABERTA = 5


def lock_loop(input_pine):
    while(GPIO.input(input_pine) == GPIO.HIGH):
        time.sleep(0.1)

def error_handler(func):
    def inner_handler(*args, **kwargs):
        global mode_motor_dc, mode_pump, state_pump, event
        event.set()

        func(*args, **kwargs)  # Error method handling

        event.clear()

        # At this point the error was fixed, continue pumps
        ativar_motor_esc(mode_motor_dc)
        ativar_bombas(mode_pump, state_pump)

    return inner_handler

def error_stop():
    while event.is_set():
        time.sleep(0.1)


### SETA A PINAGEM DA PLACA MODO BCM -> ###
###         VER PINOS COM O COMANDO gpio readall ####
GPIO.setmode(GPIO.BCM)

### FINALIZA O PROGRAMA DE FORMA SEGURA ###
def clean():
    setStep_1(0,0,0,0)
    setStep_2(0,0,0,0)
    ativar_motor_esc(False)
    ativar_bombas(True, False)
    ativar_bombas(False, False)
    GPIO.cleanup()

def sigint_handler(signum, instant):
    clean()
    update_state = threading.Thread(target=request.update_state, args=(EM_ESPERA, ))
    update_state.start()
    time.sleep(1)
    sys.exit()

signal.signal(signal.SIGINT, sigint_handler)

### PARAR TODOS OS MOTORES E BOMBAS
def stop_all(error=0):
    setStep_1(0,0,0,0)
    setStep_2(0,0,0,0)
    ativar_motor_esc(False)
    ativar_bombas(True, False)
    ativar_bombas(False, False)

############################## SENSORES ##################################
### SENSOR DE PRESANCA ####
@error_handler
def sensor_presenca_1(channel):
    if(GPIO.input(sapato_1) == GPIO.HIGH):
        threading.Thread(target=request.send_error, args=(SAPATO_DIREITA, )).start()
        print("calcado fora do local. Por favor, inserir novamente")
        stop_all()
        lock_loop(sapato_1)
        threading.Thread(target=request.clean_error).start()
        print("Erro solucionado. Ativar sistema")

@error_handler
def sensor_presenca_2(channel):
    if(GPIO.input(sapato_2) == GPIO.HIGH):
        threading.Thread(target=request.send_error, args=(SAPATO_ESQUERDA, )).start()
        print("calcado fora do local. Por favor, inserir novamente")
        stop_all()
        lock_loop(sapato_2)
        threading.Thread(target=request.clean_error).start()
        print("Erro solucionado. Ativar sistema")

### SENSOR DE NIVEL ####
@error_handler
def sensor_de_nivel_1(channel):  # Clean watter
    if(GPIO.input(nivel_1) == GPIO.HIGH):
        threading.Thread(target=request.send_error, args=(NIVEL_AGUA, )).start()
        print("NIVEL DE AGUA BAIXO, INSIRA MAIS AGUA NO SISTEMA")
        stop_all()
        lock_loop(nivel_1)
        threading.Thread(target=request.clean_error).start()
        time.sleep(0.5)
        #signal.pthread_kill(THREAD_ID, signal.SIGCONT)
        # ativar_motor_esc(True)
        #print("error sensor nivel 1")
        print("Erro solucionado. Ativar sistema")


# @error_handler
# def sensor_de_nivel_2(channel):
#     if(GPIO.input(nivel_2) == GPIO.HIGH):
#         threading.Thread(target=request.send_error, args=(NIVEL_SABAO, )).start()
#         print("NIVEL DE AGUA BAIXO, INSIRA MAIS AGUA NO SISTEMA")
#         stop_all()
#         lock_loop(nivel_2)
#         threading.Thread(target=request.clean_error).start()
#         time.sleep(0.5)
#         # ativar_motor_esc(True)
#         print("error sensor nivel 2")
#         print("Erro solucionado. Ativar sistema")

### HABILTANDO CALLBACK -> INTERRUPCOES ###
@error_handler
def my_callback(channel):
    if(GPIO.input(chave_1) == GPIO.HIGH):
        threading.Thread(target=request.send_error, args=(TAMPA_ABERTA, )).start()
        print("Motor fora dos limites. Por favor, reposicionar no inicio!")
        stop_all()
        lock_loop(chave_1)
        threading.Thread(target=request.clean_error).start()
        print("Erro solucionado. Ativar sistema")

# @error_handler
# def my_callback2(channel):
#     if(GPIO.input(chave_2) == GPIO.HIGH):
#         threading.Thread(target=request.send_error, args=(TRILHO_FORA, )).start()
#         print("Motor fora dos limites. Por favor, reposicionar no inicio!")
#         stop_all()
#         # backwards_1(15 / 1000.0, 420)
#         lock_loop(chave_2)
#         threading.Thread(target=request.clean_error).start()
#         print("Erro solucionado. Ativar sistema")


############################## MOTORES ####################################

### False - desligar ### True - ligar ###
def ativar_motor_esc(state):
    if(not state):
        GPIO.output(motor_esc_1, GPIO.HIGH)
    else:
        GPIO.output(motor_esc_1, GPIO.LOW)

def ativar_bombas(mode, state):
    if(mode):
        if(not state):
            GPIO.output(bomba_2, GPIO.HIGH)
            GPIO.output(bomba_3, GPIO.HIGH)
        else:
            GPIO.output(bomba_2, GPIO.LOW)
            GPIO.output(bomba_3, GPIO.LOW)
    elif(not mode):
        if(not state):
            GPIO.output(bomba_1, GPIO.HIGH)
        else:
            GPIO.output(bomba_1, GPIO.LOW)

def enxaguar():
    move_motor(False, 1)

def setStep_1(w1, w2, w3, w4):
    GPIO.output(m1_coil_A_1_pin, w1)
    GPIO.output(m1_coil_A_2_pin, w2)
    GPIO.output(m1_coil_B_1_pin, w3)
    GPIO.output(m1_coil_B_2_pin, w4)

def setStep_2(w1, w2, w3, w4):
    GPIO.output(motor_2_coil_A_1_pin, w1)
    GPIO.output(motor_2_coil_A_2_pin, w2)
    GPIO.output(motor_2_coil_B_1_pin, w3)
    GPIO.output(motor_2_coil_B_2_pin, w4)

#### (True, True)      --> Ligar bomba com sabao     ####
#### (True, False)     --> Desligar bomba com sabao  ####
#### (False, True)     --> Ligar bomba com agua      ####
#### (False, Flase)    --> Desligar bomba com agua   ####
def move_motor(mode, reps):
    global mode_pump, state_pump, mode_motor_dc
    #iniciando posicao para limpeza
    mode_motor_dc = True            # mode julia
    ativar_motor_esc(mode_motor_dc) # mode julia
    # forward_1(15 / 1000.0, 100)
    # mode_motor_dc = True
    # ativar_motor_esc(mode_motor_dc)

    mode_pump = mode
    state_pump = True
    ativar_bombas(mode_pump, state_pump)

    for i in range(0, reps * 4):
        print(i)
        forward_1(15 / 1000.0, 350)
        backwards_1(15 / 1000.0, 10)
        steps_2 = 6*(i+1)
        print (steps_2)
        forward_2(15 / 1000.0, steps_2)
    #voltando para posicao inicial
    mode_motor_dc = False
    ativar_motor_esc(mode_motor_dc)

    mode_pump = mode
    state_pump = False

    ativar_bombas(mode_pump, state_pump)
    backwards_1(15 / 1000.0, 0)

def forward_1(delay, steps):
    global actually_steps_motor_1
    aux_steps = actually_steps_motor_1
    for i in range(aux_steps, steps):
        error_stop()  # Only stop is a error has occured
        #print (i)
        setStep_1(1, 0, 1, 0)
        time.sleep(delay)
        setStep_1(0, 1, 1, 0)
        time.sleep(delay)
        setStep_1(0, 1, 0, 1)
        time.sleep(delay)
        setStep_1(1, 0, 0, 1)
        time.sleep(delay)
        actually_steps_motor_1 = i

def backwards_1(delay, steps):
     global actually_steps_motor_1
     aux_steps = actually_steps_motor_1
     for i in range(aux_steps, steps, -1):
        error_stop()  # Only stop is a error has occured
        #print (i)
        setStep_1(1, 0, 0, 1)
        time.sleep(delay)
        setStep_1(0, 1, 0, 1)
        time.sleep(delay)
        setStep_1(0, 1, 1, 0)
        time.sleep(delay)
        setStep_1(1, 0, 1, 0)
        time.sleep(delay)
        actually_steps_motor_1 = i

def forward_2(delay, steps):
    setStep_1(0, 0, 0, 0)
    global actually_steps_motor_2
    aux_steps = actually_steps_motor_2
    for i in range(aux_steps, steps):
        error_stop()  # Only stop is a error has occured
        # print (i)
        setStep_2(1, 0, 1, 0)
        time.sleep(delay)
        setStep_2(0, 1, 1, 0)
        time.sleep(delay)
        setStep_2(0, 1, 0, 1)
        time.sleep(delay)
        setStep_2(1, 0, 0, 1)
        time.sleep(delay)
        actually_steps_motor_2 = i

def backwards_2(delay, steps):
     setStep_1(0, 0, 0, 0)
     global actually_steps_motor_2
     aux_steps = actually_steps_motor_2
     for i in range(aux_steps, steps, -1):
        error_stop()  # Only stop is a error has occured
        # print (i)
        setStep_2(1, 0, 0, 1)
        time.sleep(delay)
        setStep_2(0, 1, 0, 1)
        time.sleep(delay)
        setStep_2(0, 1, 1, 0)
        time.sleep(delay)
        setStep_2(1, 0, 1, 0)
        time.sleep(delay)
        actually_steps_motor_2 = i


############## PINOS DOS SENSORES ################

### PINOS DOS MOTORES  ###

#MOTOR 1
m1_coil_A_1_pin = 12
m1_coil_A_2_pin = 16
m1_coil_B_1_pin = 20
m1_coil_B_2_pin = 21

#MOTOR 2
motor_2_coil_A_1_pin = 6
motor_2_coil_A_2_pin = 13
motor_2_coil_B_1_pin = 19
motor_2_coil_B_2_pin = 26

### CHAVE DE FIM DE CURSO ###
# initiallize_buttom = 8
chave_1 = 23
initiallize_buttom = 24

### SENSOR DE DISTANCIA ###
sapato_1 = 18
sapato_2 = 25

### SENSOR DE NIVEL ###
nivel_1 = 27
valvula_eletronica = 17

### MOTOR DC ####
motor_esc_1 = 22

### BOMBAS DE AGUA ####
bomba_1 = 0
bomba_2 = 1
bomba_3 = 5

### Setando pinos como entrada e saida ###
GPIO.setup(nivel_1, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.setup(initiallize_buttom, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.setup(chave_1, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(initiallize_buttom, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.setup(sapato_1, GPIO.IN)
GPIO.setup(sapato_2, GPIO.IN)

GPIO.setup(m1_coil_A_1_pin, GPIO.OUT)
GPIO.setup(m1_coil_A_2_pin, GPIO.OUT)
GPIO.setup(m1_coil_B_1_pin, GPIO.OUT)
GPIO.setup(m1_coil_B_2_pin, GPIO.OUT)

GPIO.setup(motor_2_coil_A_1_pin, GPIO.OUT)
GPIO.setup(motor_2_coil_A_2_pin, GPIO.OUT)
GPIO.setup(motor_2_coil_B_1_pin, GPIO.OUT)
GPIO.setup(motor_2_coil_B_2_pin, GPIO.OUT)

GPIO.setup(motor_esc_1, GPIO.OUT)

GPIO.setup(bomba_1, GPIO.OUT)
GPIO.setup(bomba_2, GPIO.OUT)
GPIO.setup(bomba_3, GPIO.OUT)

# VALVULA
GPIO.setup(valvula_eletronica, GPIO.OUT)

### ADICIONANDO A INTERRUPCAO ####
GPIO.add_event_detect(chave_1, GPIO.FALLING, callback=my_callback, bouncetime=500)
GPIO.add_event_detect(chave_2, GPIO.FALLING, callback=my_callback2, bouncetime=300)
# GPIO.add_event_detect(sapato_1, GPIO.FALLING, callback=sensor_presenca_1, bouncetime=300)
# GPIO.add_event_detect(sapato_2, GPIO.FALLING, callback=sensor_presenca_2, bouncetime=300)
GPIO.add_event_detect(nivel_1, GPIO.FALLING, callback=sensor_de_nivel_1, bouncetime=300)
GPIO.add_event_detect(nivel_2, GPIO.FALLING, callback=sensor_de_nivel_2, bouncetime=300)

#host = '192.168.25.215'
#port = 7000
#addr = (host, port)
#serv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#serv_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#serv_socket.bind(addr)
#serv_socket.listen(10)

#state = True

def run():
###### ETAPA 01 - LAVAR  ######
    print("Iniciando limpeza")
    numero_de_repeticoes = 1

    start_wash = threading.Thread(target=request.start_wash)
    start_wash.start()

    move_motor(True, numero_de_repeticoes)

###### ETAPA 02 - ENXAGAR ######
    print("Iniciando enxague")

    update_state = threading.Thread(target=request.update_state,
            args=(ENXAGUANDO, ))
    update_state.start()

    enxaguar()

##### EATAPA 03 - CHECAR PROXIMO CLIENTE ####
    print ("Aguardando proximo cliente")

    update_state = threading.Thread(target=request.update_state, args=(SECANDO, ))
    update_state.start()

    time.sleep(3)

if __name__ == '__main__':
    event = threading.Event()
    GPIO.output(valvula_eletronica, GPIO.LOW)
    time.sleep(2)
    while(True):
        if(GPIO.input(initiallize_buttom) == GPIO.HIGH):
            GPIO.output(valvula_eletronica, GPIO.HIGH)
            run()
            GPIO.output(valvula_eletronica, GPIO.LOW)
        time.sleep(0.1)
        # run()

#include <Wire.h>
#include <Ultrasonic.h>

//definindo endereco i2c da comunicacao rasp e arduino;
#define SLAVE_ADDRESS 0x04
int number[3] = {0}; // var slave

//Define os pinos do Arduino ligados ao Trigger e Echo;
//Sensor de presenca
#define PINO_TRG  7
#define PINO_ECHO 6

//Inicializa o sensor ultrasonico nos pinos especificados;
Ultrasonic ultrasonic(PINO_TRG, PINO_ECHO);

//HABILITANDO INTERRUPCAO NO PINO 2
byte sensorInterrupt = 0;  // 0 = digital pin 2
byte sensorPin       = 2;

//VALOR PARA CALIBRAR O SENSOR DE FLUXO
float calibrationFactor = 4.5;

//Variavel para a quantidade de pulsos
volatile byte pulseCount;

//Variaveis para controle de fluxo.
float flowRate;
unsigned int flowMilliLitres;
unsigned long totalMilliLitres;

unsigned long oldTime;

void setup() {

  Serial.begin(9600); // Definindo parametros da porta Serial;

  //Habilitando interrupcao como entrada.
  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);

  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);

  // inicalizando i2c --- Modo - Slave;
  Wire.begin(SLAVE_ADDRESS);
  
  // Definindo callbacks para a comunicacao i2c;
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);

  pulseCount        = 0;
  flowRate          = 0.0;
  flowMilliLitres   = 0;
  totalMilliLitres  = 0;
  oldTime           = 0;

Serial.println("Ready!");
}

void loop() {
  if(number[0] == 1){
    sensor_presenca();
    
  }
  if(number[0] == 2){
    fluxo_de_agua();
  }
  delay(1000);
}

/* ------------------------- FUNCOES ------------------------------------ */


/* --------- AGUARDANDO COMUNICACAO E ENVIO DE DADOS DA RASPBERRY.------- */
// callback dos dados recebidos;

void receiveData(int byteCount){
int i = 0;

while(Wire.available()) {
  number[i] = Wire.read();
  Serial.print("data received: ");
  Serial.println(number[i]);
  Serial.println("");
  i++;
  if(i == 1) 
    i = 0;
  }
}
/* ------------ ENVIANDO DADOS REQUISITADOS PELA RASPBERRY. ------------- */
// callback dos dados enviados

void sendData(){
Wire.write(number[2]);
}


/* ------------------------ SENSOR DE PRESENCA --------------------------- */

void sensor_presenca(){
  int i = 0;
  int cheio = 0;
  
    //Mostra os valores na serial
    Serial.print("Iniciando leitura de distancia");
    Serial.println("");
    while(!cheio){
      Serial.print("Centimetros: ");
      Serial.print( ultrasonic.Ranging(CM) );
      Serial.println( "cm" );
      delay(500);
      i++;
      if( i == 5){
        number[0] = 0;
        number[2] = 10;
        cheio = 1;
        i = 0;
      }
    }
}

/* -------------------------- SENSOR DE FLUXO ----------------------------- */

void fluxo_de_agua(){
  int cheio = 0;
  while(!cheio){
    if((millis() - oldTime) > 1000)    // Only process counters once per second
    {
      // Disable the interrupt while calculating flow rate and sending the value to
      // the host
      detachInterrupt(sensorInterrupt);
  
      // Because this loop may not complete in exactly 1 second intervals we calculate
      // the number of milliseconds that have passed since the last execution and use
      // that to scale the output. We also apply the calibrationFactor to scale the output
      // based on the number of pulses per second per units of measure (litres/minute in
      // this case) coming from the sensor.
      flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
  
      // Note the time this processing pass was executed. Note that because we've
      // disabled interrupts the millis() function won't actually be incrementing right
      // at this point, but it will still return the value it was set to just before
      // interrupts went away.
      oldTime = millis();
  
      // Divide the flow rate in litres/minute by 60 to determine how many litres have
      // passed through the sensor in this 1 second interval, then multiply by 1000 to
      // convert to millilitres.
      flowMilliLitres = (flowRate / 60) * 1000;
  
      // Add the millilitres passed in this second to the cumulative total
      totalMilliLitres += flowMilliLitres;
  
      unsigned int frac;
  
      // Print the flow rate for this second in litres / minute
      Serial.print("Vazao: ");
      Serial.print(int(flowRate));  // Print the integer part of the variable
      Serial.print("L/min");
      Serial.print("\t");       // Print tab space
  
      // Print the cumulative total of litres flowed since starting
      Serial.print("Agua utilizada: ");
      Serial.print(totalMilliLitres);
      Serial.println("mL");
      Serial.print("\t");       // Print tab space
      Serial.print(totalMilliLitres/1000);
      Serial.print("L");
  
  
      // Reset the pulse counter so we can start incrementing again
      pulseCount = 0;
  
      // Enable the interrupt again now that we've finished sending output
      attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
    }
    if(totalMilliLitres >= 500){
      cheio = 1;
      number[0] = 0;
      number[2] = 10;
      totalMilliLitres = 0;
    }
  }
}

/* -- CONTADOR DE PULSOS POR INTERRUOPCAO HABILITADO NO PINO 2 DO SENSOR DE FLUXO --*/
void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}

import smbus
import time
import sys
# for RPI version 1, use "bus = smbus.SMBus(0)"
bus = smbus.SMBus(1)

# This is the address we setup in the Arduino Program
address = 0x04

def writeNumber(value):
    bus.write_byte(address, value)
    # bus.write_byte_data(address, 0, value)
    return -1

def readNumber():
    number = bus.read_byte(address)
    # number = bus.read_byte_data(address, 1)
    return number

length = len(sys.argv)

msg_rpi = ""

for x in range (1,length):
    var = sys.argv[x]
    writeNumber(int(var))
    print sys.argv[x]
    msg_rpi += sys.argv[x] + ' '


print "RPI: ", msg_rpi 
# sleep one second
time.sleep(1)

number = readNumber()

#print "Arduino: ", number
#print

stepok = 0
while(stepok != 10):
    stepok = readNumber()
    time.sleep(1)
    if stepok == 10:
        print "Operacao concluida!"
    else: continue

